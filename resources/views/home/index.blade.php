@extends('layouts.app')

@section('title', 'home')

@section('conteudo')
<div class="container">
    <article class="row">
        
        <div class="col-md-4">
            <img class="img-fluid" src="https://via.placeholder.com/400x250">
        </div>
        <div class="col-md-8">
            <h2>Noticia Destaque</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facere unde doloremque quae quis repellendus,
                fuga nemo quibusdam maxime beatae voluptatum dignissimos, et corrupti blanditiis at rem esse, voluptatem
                veritatis Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, beatae quos repellat
                distinctio tempora quaerat, doloremque, omnis adipisci officia non aliquid unde libero! Vitae, pariatur?
                Quam reiciendis ipsa tenetur nihil?>
        </div>
    </article>
    <div class="row">

        @for($i= 1; $i <= 3; $i++)
        <div class="col-md-4 mt-5">
            <article class="card">
                <a href="#">
                    <img class="img-fluid" src="https://via.placeholder.com/500x250">
                </a>
                <div class="card-body">
                    <h2 class="card-text"><a href="#">Titulo de Noticia</a></h2>
                    <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae tempore, dolor
                        nihil ipsum officiis quam, rem veniam molestiae eligendi nam, ipsam maiores at facere error
                        totam eos est sint voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste,
                        vel quis debitis praesentium eligendi quae. Beatae quo tenetur dolor ipsum alias distinctio
                        atque eius? Voluptate repudiandae inventore earum. Dolore, velit?</p>


                </div>
                <div class="card-footer">
                    30/04/2019
                </div>
            </article>


        </div>

        @endfor


    </div>


</div>


@endsection
