@extends('layouts.app')
@section('titulo','Contato')



@section('conteudo')


    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                    <h2>Contato</h2>

                    <p>Faça sua sugestão, criticas ou tire suas dúvidas sobre algum conteúdo publicado.</p>
            <form>
                    <div class="form-group">
                      <label for="nome">Nome</label>
                      <input type="text" class="form-control" placeholder="digite o nome">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" placeholder="digite o email">
                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" class="form-control" placeholder="(00)0000-0000">
                    <div class="form-group">
                      <label for="assunto">Assunto</label>
                      <input type="text" class="form-control" placeholder="digite o assunto">
                    
                    <div class="form-group">
                      <label for="mensagem">Mensagem</label>
                      <textarea class="form-control" placeholder="digite a Mensagem" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-danger">enviar</button>
                  </form>



            </div>
        </div>
    </div>




@endsection